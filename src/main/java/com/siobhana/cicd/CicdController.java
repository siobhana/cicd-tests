package com.siobhana.cicd;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class CicdController {

	@RequestMapping("/")
	public String index() {
		return "Hello World";
	}

}
